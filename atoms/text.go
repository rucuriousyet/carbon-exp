package atoms

type FontWeight string

type Text struct {
	Content string
	Size    uint16
	Family  string
	Weight  FontWeight
	X, Y    int32
	Color
}

func (t Text) Type() AtomType {
	return TextAtom
}
