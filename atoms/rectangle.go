package atoms

type Rectangle struct {
	X, Y, W, H int32
	Color
}

type Rect = Rectangle

func (r Rect) Type() AtomType {
	return RectangleAtom
}
