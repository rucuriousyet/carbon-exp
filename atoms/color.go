package atoms

type Color struct {
	R, G, B, A uint8
}
