package atoms

type AtomType string

const (
	RectangleAtom AtomType = "rectangle"
	TextAtom      AtomType = "text"
)

type Atom interface {
	Type() AtomType
}
