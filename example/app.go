package main

import (
	"fmt"
	"math/rand"
	"os"
	"time"

	"gitlab.com/rucuriousyet/carbon"
	"gitlab.com/rucuriousyet/carbon/atoms"
	"gitlab.com/rucuriousyet/carbon/engine"
)

type Div struct {
}

func (d Div) Atoms() []atoms.Atom {
	return []atoms.Atom{
		atoms.Rect{
			X: 0, Y: 0, W: 50, H: 50,
			Color: atoms.Color{R: 255},
		},
		atoms.Rect{
			X: 50, Y: 0, W: 50, H: int32(rand.Intn(400)),
			Color: atoms.Color{G: 255},
		},
		atoms.Rect{
			X: 100, Y: 0, W: 50, H: int32(rand.Intn(400)),
			Color: atoms.Color{B: 255},
		},
		atoms.Text{
			X:       100,
			Y:       100,
			Content: "apples",
			Size:    32,
			Color:   atoms.Color{R: 200, B: 100},
		},
	}
}

func (d Div) Effects() []func(engine.Context) func(engine.Context) {
	return nil
}

func Run(ctx engine.Context) engine.Component {
	return Div{}
}

func main() {
	app := carbon.New()
	rand.Seed(time.Now().UnixNano())

	err := app.Start(Run)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
}
