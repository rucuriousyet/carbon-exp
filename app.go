package carbon

import (
	"fmt"
	"time"

	"gitlab.com/rucuriousyet/carbon/atoms"
	"gitlab.com/rucuriousyet/carbon/backend/sdl2"
	"gitlab.com/rucuriousyet/carbon/engine"
)

type Entrypoint func(engine.Context) engine.Component

type App struct {
	engine.Engine
	exit chan struct{}
}

func New() *App {
	return &App{
		Engine: &sdl2.Engine{},
		exit:   make(chan struct{}),
	}
}

func (a *App) Close() {
	a.exit <- struct{}{}
}

func (a *App) Start(entrypoint Entrypoint) error {
	err := a.Engine.Init()
	if err != nil {
		return err
	}
	defer a.Engine.Destroy()

	ctx := engine.RootContext(a.Engine)
	store := ctx.Store

	var emptyTime time.Time
	var lastFlush time.Time
run:
	for {
		// check if the called called Close()
		select {
		case <-a.exit:
			break run
		default:
		}

		// poll the engine for events
		event, err := a.Engine.Poll()
		if err != nil {
			if err != engine.ErrNoEvents {
				return err
			}
		}

		if event.Type() == engine.Quit {
			break run
		}

		if event.Type() == engine.Click {
			store.Flush()
		}
		//
		// if event.Type() == engine.WindowResized {
		// 	a.Engine.Clear()
		// 	store.Flush()
		// }

		// if this is the first run, or changes have been flushed to the store
		if store.LastFlush() == emptyTime || store.LastFlush() != lastFlush {
			err = a.Engine.Clear()
			if err != nil {
				return nil
			}

			// run the application code
			allAtoms := entrypoint(ctx).Atoms()
			for _, atom := range allAtoms {
				switch atom.Type() {
				case atoms.TextAtom:
					a.Engine.DrawText(atom.(atoms.Text))
					break
				case atoms.RectangleAtom:
					a.Engine.DrawRect(atom.(atoms.Rect))
					break
				}
			}
			store.Flush()

			// todo have app run return widgetTree, run differencing
			// to minimize wasted reloading

			err = a.Engine.Flush()
			if err != nil {
				return nil
			}

			// update flush time
			lastFlush = store.LastFlush()
			fmt.Println("render")
		}

		// handle resize/window events refiring render

		// loop at ~60 fps
		time.Sleep(time.Millisecond * 16)
	}

	return nil
}
