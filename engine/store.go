package engine

import (
	// "sync"
	"time"
)

type Store struct {
	// lock *sync.Mutex
	data map[string]interface{}
	//
	// cacheLock *sync.Mutex
	// cache     map[string]interface{}
	lastFlush time.Time
}

func (s *Store) Set(key string, value interface{}) {
	s.data[key] = value
}

func (s *Store) Get(key string) interface{} {
	val, ok := s.data[key]
	if !ok {
		return nil
	}

	return val
}

func (s *Store) Flush() time.Time {
	s.lastFlush = time.Now()
	return s.lastFlush
}

func (s *Store) LastFlush() time.Time {
	return s.lastFlush
}
