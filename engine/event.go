package engine

import "time"
import "strconv"

type EventType uint
func (e EventType) String() string { return strconv.Itoa(int(e)) }
const (
	Null EventType = 0
	Quit EventType = 1
	Click EventType = 2
	WindowResized EventType = 3
)

type Event interface {
	FiredAt() time.Time
	Type() EventType
}

type QuitEvent struct{}

func (e QuitEvent) FiredAt() time.Time {
	return time.Now()
}

func (e QuitEvent) Type() EventType {
	return Quit
}


type NullEvent struct{}

func (e NullEvent) FiredAt() time.Time {
	return time.Now()
}

func (e NullEvent) Type() EventType {
	return Null
}

type ClickEvent struct{}

func (e ClickEvent) FiredAt() time.Time {
	return time.Now()
}

func (e ClickEvent) Type() EventType {
	return Click
}


type WindowResizeEvent struct{}

func (e WindowResizeEvent) FiredAt() time.Time {
	return time.Now()
}

func (e WindowResizeEvent) Type() EventType {
	return WindowResized
}
