package engine

import "errors"
import "gitlab.com/rucuriousyet/carbon/atoms"

var ErrNoEvents = errors.New("no events found")

type Engine interface {
	Init() error
	// ShouldExit() bool TODO return quit event instead
	Destroy()

	RenderBackend
	EventBackend
}

type EventQueue struct {
}

func (e *EventQueue) Fire()            {}
func (e *EventQueue) RegisterHandler() {}

type EventBackend interface {
	// checks for events and maps them back to callbacks
	Poll() (Event, error) // TODO change to return engine.Event
}

type RenderBackend interface {
	// TODO add push function for all primitives
	DrawRect(atoms.Rect)
	DrawText(atoms.Text)
	Clear() error
	Flush() error
}

func BufferedRenderBackend(destination RenderBackend) RenderBackend {
	return destination
}
