package engine

import "fmt"

type Context struct {
	parentId string
	numStateCalls uint64

	EventQueue
	RenderBackend
	*Store
	*NamedStore
}

func RootContext(engine Engine) Context {
	return Context{
		parentId: "root",
		// EventQueue: &engine,
		RenderBackend: engine,
		Store: &Store{data: map[string]interface{}{}},
		// NamedStore:
	}
}

func (c *Context) Derive(childId string) Context {
	return Context{
		parentId: childId,
		EventQueue: c.EventQueue,
		RenderBackend: c.RenderBackend,
		Store: &Store{},
		NamedStore: c.NamedStore,
	}
}

func (c *Context) UseState(initVal interface{}) (interface{}, func(interface{})) {
	key := fmt.Sprintf("%s_c%d", c.parentId, c.numStateCalls)
	setter := func(updatedVal interface{}) {
		c.Store.Set(key, updatedVal)
	}

	if c.Store.Get(key) == nil {
		c.Store.Set(key, initVal)
		c.numStateCalls++

		return initVal, setter
	}

	return c.Store.Get(key), setter
}
