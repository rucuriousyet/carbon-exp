package engine

import "gitlab.com/rucuriousyet/carbon/atoms"

type Component interface {
	Atoms() []atoms.Atom
	Effects() []func(Context) func(Context)
}
