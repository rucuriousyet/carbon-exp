package engine

import (
	"sync"
	"time"
)

// NamedStore implements a global flux store
type NamedStore struct {
	lock *sync.Mutex
	data map[string]interface{}

	cacheLock *sync.Mutex
	cache     map[string]interface{}
	lastFlush time.Time
}
