package sdl2

import (
	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
	"gitlab.com/rucuriousyet/carbon/atoms"
	"gitlab.com/rucuriousyet/carbon/engine"
)

type Engine struct {
	shouldExit bool
	window     *sdl.Window
	renderer   *sdl.Renderer
	fonts      map[string]*ttf.Font
}

func (s *Engine) Init() error {
	s.fonts = map[string]*ttf.Font{}
	if err := ttf.Init(); err != nil {
		panic(err)
	}

	if err := sdl.Init(sdl.INIT_EVERYTHING); err != nil {
		return err
	}

	window, err := sdl.CreateWindow("test", sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
		800, 600, sdl.WINDOW_SHOWN)
	if err != nil {
		return err
	}
	s.window = window
	window.SetResizable(true)

	s.renderer, err = sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED&sdl.RENDERER_PRESENTVSYNC)
	if err != nil {
		return err
	}

	return nil
}

func (s *Engine) Destroy() {
	sdl.Quit()
	ttf.Quit()
	s.window.Destroy()
	s.renderer.Destroy()
}

func (s *Engine) Clear() error {
	err := s.renderer.SetDrawColor(0, 0, 0, 0)
	if err != nil {
		return err
	}

	return s.renderer.Clear()
}

func (s *Engine) Flush() error {
	s.renderer.Present()
	return nil
}

func (s *Engine) Poll() (engine.Event, error) {
	event := sdl.PollEvent()
	if event != nil {
		switch e := event.(type) {
		case *sdl.QuitEvent:
			return engine.QuitEvent{}, nil
		case *sdl.MouseButtonEvent:
			if e.State == sdl.RELEASED && e.Button == sdl.BUTTON_LEFT {
				return engine.ClickEvent{}, nil
			}
		case *sdl.WindowEvent:
			if e.Type == sdl.WINDOWEVENT_RESIZED {
				return engine.WindowResizeEvent{}, nil
			}
		}
	}

	return engine.NullEvent{}, engine.ErrNoEvents
}

func (s *Engine) Font(name string) (*ttf.Font, error) {
	font, ok := s.fonts[name]
	if !ok {
		rubik, err := ttf.OpenFont("./assets/RobotoMono-Bold.ttf", 33)
		if err != nil {
			return nil, err
		}

		s.fonts[name] = rubik
		return rubik, nil
	}

	return font, nil
}

func (s *Engine) DrawRect(rect atoms.Rect) {
	s.renderer.SetDrawColor(rect.Color.R, rect.Color.G, rect.Color.B, rect.Color.A)
	s.renderer.FillRect(&sdl.Rect{X: int32(rect.X), Y: int32(rect.Y), W: int32(rect.W), H: int32(rect.H)})
}

func (s *Engine) DrawText(text atoms.Text) {
	rubik, _ := s.Font("rubik")

	width, height, err := rubik.SizeUTF8(text.Content)
	if err != nil {
		panic(err)
	}

	// this is where to panic occurs
	textSurf, err := rubik.RenderUTF8Blended(text.Content, sdl.Color{R: text.R, G: text.G, B: text.B, A: text.A})
	if err != nil {
		panic(err)
	}

	textTexture, err := s.renderer.CreateTextureFromSurface(textSurf)
	if err != nil {
		panic(err)
	}

	s.renderer.Copy(
		textTexture,
		&sdl.Rect{
			X: 0,
			Y: 0,
			W: int32(width),
			H: int32(height),
		},
		&sdl.Rect{
			X: text.X,
			Y: text.Y,
			W: int32(width),
			H: int32(height),
		},
	)
}
